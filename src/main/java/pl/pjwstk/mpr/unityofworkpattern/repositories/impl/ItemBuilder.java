/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unityofworkpattern.repositories.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import pl.pjwstk.mpr.unitofworkpattern.domain.Item;

/**
 *
 * @author radek
 */
public class ItemBuilder implements IEntityBuilder<Item> {

    @Override
    public Item build(ResultSet rs) throws SQLException {

        Item item = new Item();
        item.setId(rs.getInt("id"));
        item.setName(rs.getString("name"));
        item.setDescription(rs.getString("description"));
        item.setPrice(rs.getDouble("price"));
        
        return item;
    }
    
}
