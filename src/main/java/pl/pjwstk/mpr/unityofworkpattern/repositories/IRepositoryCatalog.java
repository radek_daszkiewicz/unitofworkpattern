/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unityofworkpattern.repositories;

import pl.pjwstk.mpr.unitofworkpattern.domain.Address;
import pl.pjwstk.mpr.unitofworkpattern.domain.Client;
import pl.pjwstk.mpr.unitofworkpattern.domain.Item;
import pl.pjwstk.mpr.unitofworkpattern.domain.Order;

/**
 *
 * @author radek
 */
public interface IRepositoryCatalog {
    
    public IRepository<Client> getClients();
    public IRepository<Item> getItems();
    public IRepository<Address> getAddresses();
    public IRepository<Order> getOrders();
    public void commit();
}
