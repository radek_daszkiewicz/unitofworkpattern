/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unityofworkpattern.repositories.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import pl.pjwstk.mpr.unitofworkpattern.domain.Entity;
import pl.pjwstk.mpr.unitofworkpattern.unitofwork.IUnitOfWork;
import pl.pjwstk.mpr.unitofworkpattern.unitofwork.IUnitOfWorkRepository;
import pl.pjwstk.mpr.unityofworkpattern.repositories.IRepository;

/**
 *
 * @author radek
 */
public abstract class Repository<TEntity extends Entity> implements IRepository<TEntity>, IUnitOfWorkRepository {
    
    protected IUnitOfWork uow;
    protected Connection connection;
    protected PreparedStatement selectByID;
    protected PreparedStatement insert;
    protected PreparedStatement delete;
    protected PreparedStatement update;
    protected PreparedStatement selectAll;
    protected IEntityBuilder<TEntity> builder;
    
//    private PreparedStatement selectItemsByOrderId;
    
    protected String selectByIdSql = "SELECT * FROM" + getTableName() + "WHERE id=?";
    protected String deleteByIdSql = "DELETE FROM" + getTableName() + "WHERE id=?";
    protected String selectAllSql = "SELECT * FROM" + getTableName();
//    protected String selectItemsByOrderIdStmt = "SELECT * FROM order_item WHERE order_id=?";
    
    protected Repository(Connection connection, IEntityBuilder<TEntity> builder, IUnitOfWork uow) {
        
        this.uow = uow;
        this.builder = builder;
        this.connection = connection;
        try {
            selectByID = connection.prepareStatement(selectByIdSql);
            insert = connection.prepareStatement(getInsertQuery());
            delete = connection.prepareStatement(deleteByIdSql);
            update = connection.prepareStatement(getUpdateQuery());
            selectAll = connection.prepareStatement(selectAllSql);
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public TEntity withId(int id) {
        
        try {
            selectByID.setInt(1, id);
            ResultSet rs = selectByID.executeQuery();
            while(rs.next()) {
                
                return builder.build(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void add(TEntity entity) {
        uow.markAsNew(entity, this);
    }

    @Override
    public void modify(TEntity entity) {
        uow.markAsDirty(entity, this);
    }

    @Override
    public void remove(TEntity entity) {
        uow.markAsDeleted(entity, this);
    }

    @Override
    public List<TEntity> getAll() {
        List<TEntity> result = new ArrayList<TEntity>();
        
        try {
            ResultSet rs = selectAll.executeQuery();
            while(rs.next()) {
                
                result.add(builder.build(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void persistAdd(Entity entity) {
        
        try {
            
            setUpInsertQuery((TEntity) entity);
            insert.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void persistUpdate(Entity entity) {
        
        try {
            setUpUpdateQuery((TEntity)entity);
            update.executeUpdate();
            
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void persistDelete(Entity entity) {
        
        try {
            
            delete.setInt(1, entity.getId());
            delete.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }
    
    protected abstract void setUpUpdateQuery(TEntity entity) throws SQLException;
    protected abstract void setUpInsertQuery(TEntity entity) throws SQLException;
    protected abstract String getTableName();
    protected abstract String getUpdateQuery();
    protected abstract String getInsertQuery();
    
}
