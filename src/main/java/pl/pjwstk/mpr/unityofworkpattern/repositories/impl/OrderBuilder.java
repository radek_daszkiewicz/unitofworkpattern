/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unityofworkpattern.repositories.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import pl.pjwstk.mpr.unitofworkpattern.domain.Address;
import pl.pjwstk.mpr.unitofworkpattern.domain.Client;
import pl.pjwstk.mpr.unitofworkpattern.domain.Item;
import pl.pjwstk.mpr.unitofworkpattern.domain.Order;
import pl.pjwstk.mpr.unitofworkpattern.unitofwork.UnitOfWork;

/**
 *
 * @author radek
 */
public class OrderBuilder implements IEntityBuilder<Order>{
    
    private Connection connection;
    private ClientBuilder clientBuilder;
    private AddressBuilder addressBuilder;
    private ItemBuilder itemBuilder;
    private OrderItemsBuilder orderItemsBuilder;
    private UnitOfWork uow;

    public OrderBuilder(Connection connection, ClientBuilder clientBuilder, AddressBuilder addressBuilder, ItemBuilder itemBuilder, OrderItemsBuilder orderItemsBuilder, UnitOfWork uow) {
        this.connection = connection;
        this.clientBuilder = clientBuilder;
        this.addressBuilder = addressBuilder;
        this.itemBuilder = itemBuilder;
        this.uow = uow;
    }
    
    @Override
    public Order build(ResultSet rs) throws SQLException {

        Order o = new Order();
        o.setId(rs.getInt("id"));
        
        ClientRepository cr = new ClientRepository(connection, clientBuilder, uow);
        Client c = cr.withId(rs.getInt("client_id"));
        o.setClient(c);
        
        AddressRepository ar = new AddressRepository(connection, addressBuilder, uow);
        Address a = ar.withId(rs.getInt("address_id"));
        o.setDeliveryAddress(a);
        
        OrderItemsRepository oir = new OrderItemsRepository(connection, orderItemsBuilder, uow);
        List<Item> items = oir.getItemsByOrderId(rs.getLong("id"));
        o.setItems(items);
        
        return o;
    }
}
