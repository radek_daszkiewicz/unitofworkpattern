/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unityofworkpattern.repositories.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import pl.pjwstk.mpr.unitofworkpattern.domain.Address;

/**
 *
 * @author radek
 */
public class AddressBuilder implements IEntityBuilder<Address> {
    
    @Override
    public Address build(ResultSet rs) throws SQLException {
        
        Address address = new Address();
        address.setId(rs.getInt("id"));
        address.setCity(rs.getString("city"));
        address.setStreet(rs.getString("street"));
        address.setNumber(rs.getString("number"));
        return address;
    }
}
