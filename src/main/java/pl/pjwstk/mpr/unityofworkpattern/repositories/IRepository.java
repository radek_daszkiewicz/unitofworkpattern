package pl.pjwstk.mpr.unityofworkpattern.repositories;

import java.util.List;


public interface IRepository<TEntity> {

	public TEntity withId(int id);
	public void add(TEntity entity);
	public void modify(TEntity entity);
	public void remove(TEntity entity);
        public List<TEntity> getAll();
}
