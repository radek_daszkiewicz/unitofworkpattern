/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unityofworkpattern.repositories.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import pl.pjwstk.mpr.unitofworkpattern.domain.OrderItems;

/**
 *
 * @author radek
 */
public class OrderItemsBuilder implements IEntityBuilder<OrderItems> {

    @Override
    public OrderItems build(ResultSet rs) throws SQLException {
        
        OrderItems oi = new OrderItems();
        oi.setId(rs.getInt("id"));
        oi.setOrder_id(rs.getLong("order_id"));
        oi.setItem_id(rs.getLong("item_id"));
        return oi;
    }
    
}
