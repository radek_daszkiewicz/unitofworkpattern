/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unityofworkpattern.repositories.impl;

import java.sql.Connection;
import pl.pjwstk.mpr.unitofworkpattern.domain.Address;
import pl.pjwstk.mpr.unitofworkpattern.domain.Client;
import pl.pjwstk.mpr.unitofworkpattern.domain.Item;
import pl.pjwstk.mpr.unitofworkpattern.unitofwork.IUnitOfWork;
import pl.pjwstk.mpr.unityofworkpattern.repositories.IRepository;
import pl.pjwstk.mpr.unityofworkpattern.repositories.IRepositoryCatalog;

/**
 *
 * @author radek
 */
public class RepositoryCatalog implements IRepositoryCatalog {
    
    private Connection connection;
    private IUnitOfWork uow;
    
    public RepositoryCatalog(Connection connection, IUnitOfWork uow) {
        super();
        this.connection = connection;
        this.uow = uow;
    }

    @Override
    public IRepository<Client> getClients() {
        return new ClientRepository(connection, new ClientBuilder(), uow);
    }

    @Override
    public IRepository<Item> getItems() {
        return new ItemRepository(connection, new ItemBuilder(), uow);
    }
    
    @Override
    public IRepository<Address> getAddresses() {
        return new AddressRepository(connection, new AddressBuilder(), uow);
    }
    
    @Override
    public void commit() {
        uow.commit();
    }

}
