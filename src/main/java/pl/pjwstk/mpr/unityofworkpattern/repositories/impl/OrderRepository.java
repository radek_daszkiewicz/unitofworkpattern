/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unityofworkpattern.repositories.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import pl.pjwstk.mpr.unitofworkpattern.domain.Item;
import pl.pjwstk.mpr.unitofworkpattern.domain.Order;
import pl.pjwstk.mpr.unitofworkpattern.domain.OrderItems;
import pl.pjwstk.mpr.unitofworkpattern.unitofwork.IUnitOfWork;

/**
 *
 * @author radek
 */
public class OrderRepository extends Repository<Order> {
    
     IEntityBuilder<OrderItems> orderItemsBuilder;

    public OrderRepository(Connection connection, IEntityBuilder<Order> builder, IUnitOfWork uow) {
        super(connection, builder, uow);
    }

    @Override
    protected void setUpUpdateQuery(Order entity) throws SQLException {
        
        update.setLong(1, entity.getClient().getId());
        update.setLong(2, entity.getDeliveryAddress().getId());
        update.setLong(3, entity.getId());
    }

    @Override
    protected void setUpInsertQuery(Order entity) throws SQLException {
        insert.setLong(1, entity.getClient().getId());
        insert.setLong(2, entity.getDeliveryAddress().getId());
    }
    
    @Override
    protected String getTableName() {
        
        return "order";
    }

    @Override
    protected String getUpdateQuery() {
        
        return "UPDATE order SET(client_id, address_id)=(?, ?) WHERE id=?";
    }

    @Override
    protected String getInsertQuery() {
        
        return "INSERT INTO order(client_id, address_id) VALUES(?, ?)";
    }
}
