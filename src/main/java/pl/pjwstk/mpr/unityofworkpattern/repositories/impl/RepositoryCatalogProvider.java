/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unityofworkpattern.repositories.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import pl.pjwstk.mpr.unitofworkpattern.unitofwork.IUnitOfWork;
import pl.pjwstk.mpr.unitofworkpattern.unitofwork.UnitOfWork;
import pl.pjwstk.mpr.unityofworkpattern.repositories.IRepositoryCatalog;

/**
 *
 * @author radek
 */
public class RepositoryCatalogProvider {
    
    private static String url = "jdbc:hsqldb:hsql//localhost/workdb";
    
    public static IRepositoryCatalog catalog() {
        
        try {
            
            Connection connection = DriverManager.getConnection(url);
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepositoryCatalog catalog = new RepositoryCatalog(connection, uow);
            
            return catalog;
        } catch(SQLException e) {
            
            e.printStackTrace();
        }
        return null;
    }
}
