/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unityofworkpattern.repositories.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import pl.pjwstk.mpr.unitofworkpattern.domain.Client;

/**
 *
 * @author radek
 */
public class ClientBuilder implements IEntityBuilder<Client> {

    @Override
    public Client build(ResultSet rs) throws SQLException {
        
        Client client = new Client();
        client.setId(rs.getInt("id"));
        client.setName(rs.getString("name"));
        client.setSurname(rs.getString("surname"));
        client.setLogin(rs.getString("login"));
        return client;
    }
}
