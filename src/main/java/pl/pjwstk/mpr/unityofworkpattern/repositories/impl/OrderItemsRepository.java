/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unityofworkpattern.repositories.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import pl.pjwstk.mpr.unitofworkpattern.domain.Item;
import pl.pjwstk.mpr.unitofworkpattern.domain.OrderItems;
import pl.pjwstk.mpr.unitofworkpattern.unitofwork.IUnitOfWork;
import pl.pjwstk.mpr.unityofworkpattern.db.IOrderItemsRepository;


/**
 *
 * @author radek
 */
public class OrderItemsRepository extends Repository<OrderItems> implements IOrderItemsRepository{

    private String createOrderItemsTable = "CREATE TABLE order_items(id BIGINT, order_id BIGINT, item_id BIGINT)";
    private String selectItemsByOrderIdStmt = "SELECT id, order_id, item_id FROM order_item WHERE order_id=?";
    private PreparedStatement selectItemsByOrderId;
    
    public OrderItemsRepository(Connection connection, IEntityBuilder<OrderItems> builder, IUnitOfWork uow) {
       
        super(connection, builder, uow);
        
        try {

            ResultSet rs = connection.getMetaData().getTables(null, null, null, null) ;
            boolean tableExists = false;
            while (rs.next()) {
                if (rs.getString("TABLE_NAME").equalsIgnoreCase("order_items")) {
                    tableExists = true;
                    break;
                }
            }
            if (!tableExists) {
                Statement createTable = connection.createStatement();
                createTable.executeUpdate(createOrderItemsTable);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void setUpUpdateQuery(OrderItems entity) throws SQLException {

        update.setLong(1, entity.getOrder_id());
        update.setLong(2, entity.getItem_id());
        update.setLong(3, entity.getOrder_id());
    }

    @Override
    protected void setUpInsertQuery(OrderItems entity) throws SQLException {

        insert.setLong(1, entity.getOrder_id());
        insert.setLong(2, entity.getItem_id());
    }

    @Override
    protected String getTableName() {

        return "order_item";
    }

    @Override
    protected String getUpdateQuery() {

        return "UPDATE order_item SET(order_id, item_id)=(?, ?) WHERE id=?";
    }

    @Override
    protected String getInsertQuery() {

        return "INSERT INTO order_item(order_id, item_id) VALUES(?, ?)";
    }

    @Override
    public List<Item> getItemsByOrderId(Long id) {

        List<Item> items = new ArrayList<Item>();
        
        try {
            
            selectItemsByOrderId.setLong(1, id);
            ResultSet rs = selectItemsByOrderId.executeQuery();
            while(rs.next()) {
                
                ItemBuilder itemBuilder = new ItemBuilder();
                Item i = itemBuilder.build(rs);
                items.add(i);
            }
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        return items;
    }
}
