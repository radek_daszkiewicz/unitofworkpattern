/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unityofworkpattern.repositories.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import pl.pjwstk.mpr.unitofworkpattern.domain.Entity;

/**
 *
 * @author radek
 */
public interface IEntityBuilder<TEntity extends Entity> {
    
    public TEntity build(ResultSet rs) throws SQLException;
}
