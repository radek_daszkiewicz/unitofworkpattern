/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unityofworkpattern.db;

import java.util.List;
import pl.pjwstk.mpr.unitofworkpattern.domain.Address;
import pl.pjwstk.mpr.unityofworkpattern.repositories.IRepository;

/**
 *
 * @author radek
 */
public interface IAddressRepository extends IRepository<Address>{
    public List<Address> withCity(String name);
}
