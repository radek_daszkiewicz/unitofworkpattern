/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unitofworkpattern.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author radek
 */
public class Order extends Entity{
    
    private Client client;
    private Address deliveryAddress;
    private List<Item> items;
    
    public Order() {
        items = new ArrayList<Item>();
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
    
}