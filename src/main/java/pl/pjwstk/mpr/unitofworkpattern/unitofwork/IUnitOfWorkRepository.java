/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unitofworkpattern.unitofwork;

import pl.pjwstk.mpr.unitofworkpattern.domain.Entity;

/**
 *
 * @author radek
 */
public interface IUnitOfWorkRepository {
    
    public void persistAdd(Entity entity);
    public void persistUpdate(Entity entity);
    public void persistDelete(Entity entity);
}
