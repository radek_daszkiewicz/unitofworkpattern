/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pjwstk.mpr.unitofworkpattern.unitofwork;

import pl.pjwstk.mpr.unitofworkpattern.domain.Entity;

/**
 *
 * @author radek
 */
public interface IUnitOfWork {
    
    public void commit();
    public void rollback();
    public void markAsNew(Entity entity, IUnitOfWorkRepository repository);
    public void markAsDirty(Entity entity, IUnitOfWorkRepository repository);
    public void markAsDeleted(Entity entity, IUnitOfWorkRepository repository);
}
